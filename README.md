**Application for automatic testing if C source codes meet the assignment.**

Assignments can contain many different inputs (numbers, random numbers, strings,...) and outputs.

*planned features:*

1. Ability to use files as input or output for assignment.

2. Showing results in graph.

3. sorting source codes by efficiency (processor time).

4. ...